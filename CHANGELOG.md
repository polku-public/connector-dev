# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.9] - 2020-12-07
### Changed
- MQTT message handling.
### Added
- Connector library unit tests.
- Better array support to transformer.

## [0.0.8] - 2020-10-26
### Changed
- Connector library refactor.

## [0.0.7] - 2020-10-19
### Added
- MQTT Broker plugin with TLS support.

## [0.0.6] - 2020-10-15
### Added
- OAuth2 authorization code grant type support.
- Plugin endpoints.

## [0.0.5] - 2020-09-15
### Added
- Transformer library.

## [0.0.4] - 2020-09-14
### Changed
- Format of definitions.

## [0.0.3] - 2020-09-07
### Changed
- Improve object sorting.

## [0.0.2] - 2020-09-07
### Changed
- Change signing padding from RSA_PKCS1_PSS_PADDING to RSA_PKCS1_PADDING (default).

## [0.0.1] - 2020-09-04
### Added
- This CHANGELOG file.

[Unreleased]: https://gitlab.com/polku-public/connector-dev/-/compare/master...v0.0.9
[0.0.9]: https://gitlab.com/polku-public/connector-dev/-/releases#v0.0.9
[0.0.8]: https://gitlab.com/polku-public/connector-dev/-/releases#v0.0.8
[0.0.7]: https://gitlab.com/polku-public/connector-dev/-/releases#v0.0.7
[0.0.6]: https://gitlab.com/polku-public/connector-dev/-/releases#v0.0.6
[0.0.5]: https://gitlab.com/polku-public/connector-dev/-/releases#v0.0.5
[0.0.4]: https://gitlab.com/polku-public/connector-dev/-/releases#v0.0.4
[0.0.3]: https://gitlab.com/polku-public/connector-dev/-/releases#v0.0.3
[0.0.2]: https://gitlab.com/polku-public/connector-dev/-/releases#v0.0.2
[0.0.1]: https://gitlab.com/polku-public/connector-dev/-/releases#v0.0.1
